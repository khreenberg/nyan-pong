package com.khr.NyanPong;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.khr.NyanPong.engine.GameMain;

@SuppressWarnings( "serial" )
public class Paddle extends JPanel {

	/* Instance variables */
	// Info
	private final String WORKING_DIRECTORY = System.getProperty( "user.dir" );

	// Graphics & Positioning
	private Image paddleImage;
	private Rectangle boundingBox;
	private int x, y;
	private final int MOVE_SPEED = 5;

	// Enums
	public enum CatType {
		NYAN,
		TAC
	}

	// Constructor variables.
	private int playerNumber;
	private CatType catType;

	/**
	 * Class constructor
	 * 
	 * @param player
	 *            Player number. Player 1 is generally on top, and player 2 is
	 *            in the
	 *            bottom.
	 * @param catType
	 *            Type of paddle. "NYAN cat" or "TAC nayn".
	 */
	public Paddle( int player, CatType catType ) {
		// Store the values.
		playerNumber = player;
		this.catType = catType;
		/* Construct paddle */
		// Choose the appropriate graphics
		if( catType == CatType.NYAN ){
			paddleImage = importImage( "NyanPaddle.png" );
		}else{
			paddleImage = importImage( "TacPaddle.png" );
		}
		// Select the correct position
		x = (GameMain.getWindowWidth() / 2) - (paddleImage.getWidth( null ) / 2);
		if( player == 1 ){
			y = 30;
		}else if( player == 2 ){
			y = GameMain.getWindowHeight() - 30 - paddleImage.getHeight( null );
		}
		boundingBox = new Rectangle( x, y, paddleImage.getWidth( null ), paddleImage.getHeight( null ) );
	}

	/**
	 * Paints the paddle
	 * @param g Graphics to paint on.
	 */
	public void paintComponent( Graphics g ){
		g.drawImage( paddleImage, x, y, null );
	}

	/**
	 * Moves the paddle
	 * @param direction Direction to move. "left" or "right".
	 */
	public void move( String direction ){
		if( direction == "left" && x > 0 ){
			x -= MOVE_SPEED;
		}else if( direction == "right" && x < GameMain.getWindowWidth() - paddleImage.getWidth( null ) ){
			x += MOVE_SPEED;
		}
		boundingBox.setLocation( x, y );
	}

	/**
	 * Import an image.
	 * @param imgName Image to import from /res/img/.
	 * @return null Returns null if no image is found.
	 */
	private BufferedImage importImage( String imgName ){
		try{
			return ImageIO.read( new File( WORKING_DIRECTORY + "/res/img/" + imgName ) );
		}
		catch( Exception e ){}
		return null;
	}

	/* Getters & Setters */
	/**
	 * Return the player number.
	 * @return the playerNumber
	 */
	public int getPlayerNumber(){
		return playerNumber;
	}

	/**
	 * Return the type of paddle.
	 * @return the catType
	 */
	public CatType getCatType(){
		return catType;
	}

	/**
	 * Get the x-coordinates of the top left corner of the paddle.
	 * @return the x-coordinate.
	 */
	public int getX(){
		return x;
	}

	/**
	 * Set a new x-coordinate for the paddle.
	 * @param newX The new x-coordinate.
	 */
	public void setX( int newX ){
		x = newX;
	}

	/**
	 * Get the y-coordinates of the top left corner of the paddle.
	 * @return the y-coordinate.
	 */
	public int getY(){
		return y;
	}

	/**
	 * Set a new y-coordinate for the paddle.
	 * @param newY The new y-coordinate.
	 */
	public void setY( int newY ){
		y = newY;
	}

	/**
	 * Get bounding box.
	 * @return The bounding box of the object.
	 */
	public Rectangle getBoundingBox(){
		return boundingBox;
	}
}
