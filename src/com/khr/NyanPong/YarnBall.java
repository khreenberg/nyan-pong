package com.khr.NyanPong;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.khr.NyanPong.engine.GameMain;

@SuppressWarnings( "serial" )
public class YarnBall extends JPanel {

	/* Instance variables */
	// Info
	private final String WORKING_DIRECTORY = System.getProperty( "user.dir" );

	/* Movement stuff */
	// Coordinates
	private double x, y;
	private double dX, dY;
	// Speed & direction
	private final double STARTING_SPEED = 4;
	private double speed = STARTING_SPEED;
	private final double SPEED_INCREASE = .5;
	private final int MIN_ANGLE = 15;
	private final int DELTA_ANGLE_SPAN = 60;
	private double exitAngle;
	private int horizontalDir = 1;
	private int verticalDir = 1;
	// Counters
	private final int BALL_START_DELAY = 70;
	private int startDelay = BALL_START_DELAY;
	private final int SPEED_COUNTER_START = 3;		// Speed is increased every n..
	private int speedCounter = SPEED_COUNTER_START;	// ..times the ball hits a paddle.

	// Graphics and sounds
	private Image ballImg = importImage( "yarnball-red-18px.png" );
	private Rectangle boundingBox;
	private int ballWidth = ballImg.getWidth( null );
	private int ballHeight = ballImg.getHeight( null );

	/* Constructors */
	/**
	 * Constuct the yarnBall.
	 * @param x Starting x-coordinate.
	 * @param y Starting y-coordinate.
	 */
	public YarnBall( double x, double y ) {
		this.x = x - ballWidth / 2;
		this.y = y - ballHeight / 2;
		int roundedX = (int) Math.round( x );
		int roundedY = (int) Math.round( y );
		boundingBox = new Rectangle( roundedX, roundedY, ballWidth, ballHeight );
		exitAngle = 45;
	}

	/* Methods */
	/**
	 * Paint the yarnball.
	 * @param g Graphics to draw the ball onto.
	 */
	public void paintComponent( Graphics g ){
		int intX = (int) Math.round( x );
		int intY = (int) Math.round( y );
		g.drawImage( ballImg, intX, intY, null );
	}

	/**
	 * Move the ball.
	 */
	public void move(){
		if( startDelay > 0 ){
			startDelay-- ;
		}else{
			double newX;
			double newY;
			double radians = Math.toRadians( exitAngle );
			dX = Math.sin( radians ) * speed;
			dY = Math.cos( radians ) * speed;
			newX = x + dX * horizontalDir;
			newY = y + dY * verticalDir;

			// Update the boundingBox coordinates of the ball
			boundingBox.setLocation( (int) Math.round( newX ), (int) Math.round( newY ) );

			// Test for collisions with paddles.
			if( paddleCollision() != null ){
				exitAngle = calcAngle();
				verticalDir *= -1;
				if( speedCounter - 1 <= 0 ){
					speed += SPEED_INCREASE;
					speedCounter = SPEED_COUNTER_START;
				}else{
					speedCounter-- ;
				}
				System.out.println( exitAngle );
			}
			// Test for vertical walls.
			if( newX <= 0 || newX > GameMain.getWindowWidth() - ballWidth ){
				horizontalDir *= -1;
			}
			// Move the ball to the new coordinates.
			x = newX;
			y = newY;
			// Test for Out of Bounds
			if( y < 0 || y > GameMain.getWindowHeight() ){
				ballOOB();
			}
		}
	}

	/**
	 * Collision detection.
	 * 
	 * @return The paddle the ball has hit. 
	 * 			Returns null if there's no collision.
	 */
	private Paddle paddleCollision(){
		if( verticalDir < 0 ){
			if( boundingBox.intersects( GameMain.getP1().getBoundingBox() ) ){
				return GameMain.getP1();
			}
		}else if( verticalDir > 0 ){
			if( boundingBox.intersects( GameMain.getP2().getBoundingBox() ) ){
				return GameMain.getP2();
			}
		}
		return null;
	}

	/**
	 * Calculates new angle, when the ball hits the paddle:
	 * @return The new exit angle of the ball.
	 */
	private double calcAngle(){
		double newAngle = exitAngle;
		Paddle hitPaddle = paddleCollision();
		int paddleX = hitPaddle.getX();
		double paddleWidth = (int) Math.round( hitPaddle.getBoundingBox().getWidth() );

		int middleOfBall = (int) Math.round( this.x ) + ballWidth - 2;
		int middleOfPaddle = (int) Math.round( paddleX + paddleWidth / 2 );

		if( horizontalDir < 0 ){
			newAngle = exitAngle + (middleOfPaddle - middleOfBall) * (DELTA_ANGLE_SPAN / paddleWidth);
		}else if( horizontalDir > 0 ){
			newAngle = exitAngle + (middleOfBall - middleOfPaddle) * (DELTA_ANGLE_SPAN / paddleWidth);
		}
		if( newAngle > 90 - MIN_ANGLE ){
			newAngle = 90 - MIN_ANGLE;
		}else if( newAngle < MIN_ANGLE - 90 ){
			newAngle = MIN_ANGLE - 90;
		}
		try{
			System.out.printf(	"Delta angle = (%d - %d) * (%d / %d)",
								middleOfPaddle,
								middleOfBall,
								DELTA_ANGLE_SPAN,
								(int) paddleWidth );
			System.out.printf( " = %s\n", (middleOfBall - middleOfPaddle) * (DELTA_ANGLE_SPAN / paddleWidth) );
		}
		catch( Exception e ){}
		return newAngle;
	}

	/**
	 * Resets the ball.
	 */
	private void resetBall(){
		x = GameMain.getWindowWidth() / 2 - ballWidth / 2;
		y = GameMain.getWindowHeight() / 2 - ballHeight / 2;
		horizontalDir = Math.random() > 0.5	? 1
											: -1;
		startDelay = BALL_START_DELAY;
		speed = STARTING_SPEED;
	}

	/**
	 * Reset the ball, and increase the proper score. (OOB = Out of Bounds)
	 */
	private void ballOOB(){
		if( y < 0 ){
			GameMain.score.setP2Score( GameMain.score.getP2Score() + 1 );
		}else{
			GameMain.score.setP1Score( GameMain.score.getP1Score() + 1 );
		}
		System.out.printf( "Player 1 score: %4d\n", GameMain.score.getP1Score() );
		System.out.printf( "Player 2 score: %4d\n", GameMain.score.getP2Score() );
		resetBall();
	}

	/**
	 * Import an image.
	 * 
	 * @param imgName Image to import from /res/img/.
	 * @return null Returns null if no image is found.
	 */
	private BufferedImage importImage( String imgName ){
		try{
			return ImageIO.read( new File( WORKING_DIRECTORY + "/res/img/" + imgName ) );
		}
		catch( Exception e ){}
		return null;
	}

	/**
	 * Get the speed of the ball.
	 */
	public double getSpeed(){
		return speed;
	}
}
