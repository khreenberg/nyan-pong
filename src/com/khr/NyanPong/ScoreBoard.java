package com.khr.NyanPong;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.khr.NyanPong.engine.GameMain;

@SuppressWarnings( "serial" )
public class ScoreBoard extends JPanel {

	/* Instance Variables */
	private int p1Score, p2Score;
	private Font scoreFont = new Font( Font.MONOSPACED, Font.PLAIN, 24 );
	private Font speedFont = new Font( Font.MONOSPACED, Font.BOLD, 48 );
	private final Color fontColor = Color.DARK_GRAY;
	private final int WIDTH = GameMain.getWindowWidth();
	private final int HEIGHT = GameMain.getWindowHeight();
	private final Color speedColor = new Color( 8, 10, 10 ); 
	/* Constructors */
	public ScoreBoard() {
	}

	/* Methods */
	/**
	 * Paints the scores.
	 * @param g Graphics to paint on.
	 */
	public void paintComponent( Graphics g ){
		paintSpeed( WIDTH / 2 - 260, HEIGHT / 2, g );
		paintP1Score( 40, 64, g );
		paintP2Score( WIDTH - 225, HEIGHT - 64, g );
	}

	/**
	 * Paints the score of player one.
	 * @param x X-coordinate
	 * @param y Y-coordinate
	 * @param g Graphics to paint the score on.
	 */
	private void paintP1Score( int x, int y, Graphics g ){
		g.setColor( fontColor );
		g.setFont( scoreFont );
		g.drawString( String.format( "Player 1: %3s", p1Score ), x, y );
	}
	
	/**
	 * Paints the score of player two.
	 * @param x X-coordinate
	 * @param y Y-coordinate
	 * @param g Graphics to paint the score on.
	 */
	private void paintP2Score( int x, int y, Graphics g ){
		g.setColor( fontColor );
		g.setFont( scoreFont );
		g.drawString( String.format( "Player 2: %3s", p2Score ), x, y );
	}
	
	/**
	 * Paints the speed of the ball.
	 * @param x X-coordinate
	 * @param y Y-coordinate
	 * @param g Graphics to paint the speed on.
	 */
	private void paintSpeed( int x, int y, Graphics g ){
		g.setColor( speedColor );
		g.setFont( speedFont );
		g.drawString( String.format( "Ball speed:%4s", GameMain.getBall().getSpeed() ), x, y );
	}

	/* Getters & Setters */
	/**
	 * Set the score of player 1.
	 * @param newScore The new score of player 1.
	 */
	public void setP1Score( int newScore ){
		p1Score = newScore;
	}
	/**
	 * Get the score of player 1.
	 * @return Player 1's score.
	 */
	public int getP1Score(){
		return p1Score;
	}

	/**
	 * Set the score of player 2.
	 * @param newScore The new score of player 2.
	 */
	public void setP2Score( int newScore ){
		p2Score = newScore;
	}

	/**
	 * Get the score of player 2.
	 * @return Player 2's score.
	 */
	public int getP2Score(){
		return p2Score;
	}
}
