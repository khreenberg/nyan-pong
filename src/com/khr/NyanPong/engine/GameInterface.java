package com.khr.NyanPong.engine;

public interface GameInterface {

	/**
	 * This method keeps looping as long as the game is running.
	 */
	public void run();

	/**
	 * This method will set up everything need for the game to run
	 */
	void initialize();

	/**
	 * This method will check for input, move things
	 * around and check for win conditions, etc
	 */
	void update();

	/**
	 * This method will draw everything
	 */
	void draw();
}
