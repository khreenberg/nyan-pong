package com.khr.NyanPong.engine;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class InputHandler implements KeyListener {

	// Instance variables
	boolean[] keys = new boolean[256];;

	/**
	 * Constructs our InputHandler
	 * 
	 * @param c
	 *            Component to take input from.
	 */
	public InputHandler( Component c ) {
		c.addKeyListener( this );
	}

	/**
	 * Checks whether a key is down.
	 * 
	 * @param keyCode
	 *            The key to check for.
	 * @return If the key is pressed or not.
	 */
	public boolean isKeyDown( int keyCode ){
		if( 0 < keyCode && keyCode < 256 ){
			return keys[keyCode];
		}
		return false;
	}

	/**
	 * Checks whether a key is pressed while the component is in focus.
	 * 
	 * @param e
	 *            The KeyEvent sent by the component.
	 */
	public void keyPressed( KeyEvent e ){
		if( 0 < e.getKeyCode() && e.getKeyCode() < 256 ){
			keys[e.getKeyCode()] = true;
		}
	}

	/**
	 * Checks whether a key is released while the component is in focus.
	 * 
	 * @param e
	 *            The KeyEvent sent by the component.
	 */
	public void keyReleased( KeyEvent e ){
		if( 0 < e.getKeyCode() && e.getKeyCode() < 256 ){
			keys[e.getKeyCode()] = false;
		}
	}

	// Not used.
	public void keyTyped( KeyEvent e ){
	}

}
