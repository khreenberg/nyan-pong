package com.khr.NyanPong.engine;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.khr.NyanPong.Paddle;
import com.khr.NyanPong.ScoreBoard;
import com.khr.NyanPong.YarnBall;

@SuppressWarnings( "serial" )
public class GameMain extends JFrame implements GameInterface {

	// Window stuff
	private Insets ins;
	private static int WINDOW_WIDTH = 800;
	private static int WINDOW_HEIGHT = 600;
	private static final String GAME_TITLE = "Nyan Pong";
	private static final int FPS = 60;

	// Management stuff
	private boolean isRunning = false;
	private boolean isPaused = false;

	// Graphics stuff
	public BufferedImage backBuffer;
	public static Graphics graphics;
	public static Graphics backBufferedGraphics;

	// Instanciate the objects.
	public static ScoreBoard score = new ScoreBoard();
	private static Paddle p1 = new Paddle( 1, Paddle.CatType.NYAN );
	private static Paddle p2 = new Paddle( 2, Paddle.CatType.TAC );
	private static YarnBall ball = new YarnBall( WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 );

	// Control stuff
	public static InputHandler input;
	private int pauseDelay = 10;

	/**
	 * Starts everything.
	 */
	public static void main( String[] args ){
		GameMain game = new GameMain();
		game.run();
		System.exit( 0 );
	}

	/**
	 * Keep looping our game until we're done playing.
	 */
	public void run(){

		initialize();

		while( isRunning ){
			togglePause();
			while( !isPaused ){
				long time = System.currentTimeMillis();

				update();
				draw();

				time = (1000 / FPS) - (System.currentTimeMillis() - time);

				if( time > 0 ){
					try{
						Thread.sleep( time );
					}
					catch( Exception e ){}
				}
			}
		}

		setVisible( false );
	}

	/**
	 * Initialize the window and start the game.
	 */
	public void initialize(){
		// Window
		setTitle( GAME_TITLE );
		setSize( WINDOW_WIDTH, WINDOW_HEIGHT );
		setResizable( false );
		setDefaultCloseOperation( EXIT_ON_CLOSE );

		// Add objects
		add( score );
		add( p1 );
		add( p2 );
		add( ball );

		// Add controls
		input = new InputHandler( this );

		// Launch!
		setVisible( true );
		isRunning = true;

		// Fix insets
		ins = getInsets();
		setSize( ins.left + WINDOW_WIDTH + ins.right, ins.top + WINDOW_HEIGHT + ins.bottom );

	}

	/**
	 * Update variables, positions etc.
	 */
	public void update(){
		getInput();
		ball.move();
	}

	/**
	 * Draw the graphics.
	 */
	public void draw(){
		// Buffers & stuff
		backBuffer = new BufferedImage( WINDOW_WIDTH, WINDOW_HEIGHT, BufferedImage.TYPE_INT_RGB );

		graphics = getGraphics();
		backBufferedGraphics = backBuffer.getGraphics();

		// Background image
		backBufferedGraphics.setColor( Color.BLACK );
		backBufferedGraphics.fillRect( 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT );

		// Draw objects
		score.paintComponent( backBufferedGraphics );
		p1.paintComponent( backBufferedGraphics );
		p2.paintComponent( backBufferedGraphics );
		ball.paintComponent( backBufferedGraphics );

		// Draw the backBuffer to our screen
		graphics.drawImage( backBuffer, ins.left, ins.top, this );
	}

	// Keyboard input
	/**
	 * Takes input from the player.
	 */
	private void getInput(){
		/* Readability */
		// ArrowKey controls
		//		boolean upPressed = input.isKeyDown( KeyEvent.VK_UP );
		//		boolean downPressed = input.isKeyDown( KeyEvent.VK_DOWN );
		boolean leftPressed = input.isKeyDown( KeyEvent.VK_LEFT );
		boolean rightPressed = input.isKeyDown( KeyEvent.VK_RIGHT );
		// WASD controls
		//		boolean wPressed = input.isKeyDown( KeyEvent.VK_W );
		//		boolean sPressed = input.isKeyDown( KeyEvent.VK_S );
		boolean aPressed = input.isKeyDown( KeyEvent.VK_A );
		boolean dPressed = input.isKeyDown( KeyEvent.VK_D );

		/* Controls */
		// Player 1
		if( aPressed ){
			p1.move( "left" );
		}else if( dPressed ){
			p1.move( "right" );
		}
		// Player 2
		if( leftPressed ){
			p2.move( "left" );
		}else if( rightPressed ){
			p2.move( "right" );
		}
		// Window controls
		if( input.isKeyDown( KeyEvent.VK_ESCAPE ) ){
			isRunning = false;
			isPaused = true;
		}
		togglePause();
	}

	/**
	 * Toggles pausing of the game.
	 */
	public void togglePause(){
		if( pauseDelay < 0 ){
			if( input.isKeyDown( KeyEvent.VK_P ) ){
				if( isPaused ){
					System.out.println( "Unpaused" );
					isPaused = false;
				}else{
					System.out.println( "Paused" );
					isPaused = true;
				}
			}
			pauseDelay = 10;
		}else{
			pauseDelay-- ;
		}

	}

	// Getters & Setters
	/**
	 * Get the width of the window.
	 * @return The width of the window.
	 */
	public static int getWindowWidth(){
		return WINDOW_WIDTH;
	}

	/**
	 * Get the height of the window.
	 * @return The height of the window.
	 */
	public static int getWindowHeight(){
		return WINDOW_HEIGHT;
	}

	/**
	 * Call the reference to player 1's paddle.
	 * @return Player 1's paddle.
	 */
	public static Paddle getP1(){
		return p1;
	}

	/**
	 * Call the reference to player 2's paddle.
	 * @return Player 2's paddle.
	 */
	public static Paddle getP2(){
		return p2;
	}

	/**
	 * Call the reference to the ball.
	 * @return The YarnBall.
	 */
	public static YarnBall getBall(){
		return ball;
	}
}